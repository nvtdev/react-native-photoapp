import React from 'react';
import { Actions, Scene, Router } from 'react-native-router-flux';
import MainPage from './components/MainPage';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 55 }}>
      <Scene key="mainScene">
        <Scene
          key="main"
          component={MainPage}
          title="Home"
          rightTitle="Take Photo"
          onRight={() => Actions.takePhoto()}
        />
      </Scene>
    </Router>
  );
};

export default RouterComponent;
