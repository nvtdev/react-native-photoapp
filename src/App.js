import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    const config = {
    apiKey: 'AIzaSyCd6nmBvn6u7yytpjKu5BFJSyTUxU_afOA',
    authDomain: 'photoapp-d81c6.firebaseapp.com',
    databaseURL: 'https://photoapp-d81c6.firebaseio.com',
    projectId: 'photoapp-d81c6',
    storageBucket: '',
    messagingSenderId: '274425204824'
  };
  firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
