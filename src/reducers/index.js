import { combineReducers } from 'redux';
import EmployeeReducer from './EmployeeReducer';

export default combineReducers({
  // auth piece of state will be maintained by the AuthReducer and so on..
  employees: EmployeeReducer
});
